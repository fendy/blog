package com.wordpress.nofipriyanto.blog.dao;

import org.springframework.data.repository.CrudRepository;

import com.wordpress.nofipriyanto.blog.entity.Blog;

public interface BlogDao extends CrudRepository<Blog, String>{
    
}
