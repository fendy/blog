package com.wordpress.nofipriyanto.blog.dto;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class BlogDto {

    @NotEmpty(message = "Title tidak bisa kosong")
    private String title;

    @NotEmpty(message = "Body tidak bisa kosong")
    private String body;
}
