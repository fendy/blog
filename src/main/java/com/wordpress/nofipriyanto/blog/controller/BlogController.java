package com.wordpress.nofipriyanto.blog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wordpress.nofipriyanto.blog.dao.BlogDao;
import com.wordpress.nofipriyanto.blog.dto.BlogDto;
import com.wordpress.nofipriyanto.blog.entity.Blog;
import com.wordpress.nofipriyanto.blog.service.BlogService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api")
public class BlogController {

    @Autowired
    private BlogService bs;

    @PreAuthorize("hasAuthority('LIHAT_BLOG')")
    @GetMapping("/blog")
    public Iterable<Blog> getAll() {
        return bs.ambilSemuas();
    }

    @PreAuthorize("hasAuthority('DETAIL_BLOG')")
    @GetMapping("/blog/{id}")
    public ResponseEntity<?> getById(@PathVariable String id) {
        Blog blog = bs.ambilById(id);
        if (blog == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(blog, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('SIMPAN_BLOG')")
    @PostMapping("/blog")
    public ResponseEntity<?> simpan(@RequestBody @Valid BlogDto bdt, Errors errors) {
        if (errors.hasErrors()) {
            return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        bs.simpan(bdt);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('EDIT_BLOG')")
    @PostMapping("/blog/{id}")
    public ResponseEntity<?> edit(
            @PathVariable String id,
            @RequestBody @Valid BlogDto bdt,
            Errors errors) {
        Blog blog = bs.ambilById(id);
        if (blog == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (errors.hasErrors()) {
            return new ResponseEntity<>(errors.getAllErrors(), HttpStatus.BAD_REQUEST);
        }
        bs.edit(id, bdt, blog);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PreAuthorize("hasAuthority('HAPUS_BLOG')")
    @DeleteMapping("/blog/{id}")
    public ResponseEntity<?> hapusById(@PathVariable String id) {
        Blog blog = bs.ambilById(id);
        if (blog == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        bs.hapus(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
