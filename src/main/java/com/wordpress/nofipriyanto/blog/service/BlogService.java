package com.wordpress.nofipriyanto.blog.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wordpress.nofipriyanto.blog.dao.BlogDao;
import com.wordpress.nofipriyanto.blog.dto.BlogDto;
import com.wordpress.nofipriyanto.blog.entity.Blog;

@Service
public class BlogService {
    
    @Autowired
    private BlogDao bd;

    public Iterable<Blog> ambilSemuas(){
        return bd.findAll();
    }

    public Blog ambilById(String id){
        return bd.findById(id).orElse(new Blog());
    }

    public void simpan(BlogDto bdt){
        Blog blog = new Blog();
        blog.setTitle(bdt.getTitle());
        blog.setBody(bdt.getBody());
        blog.setAuthor("admin");
        bd.save(blog);
    }

    public void edit(String id, BlogDto bdt, Blog blog){
        blog.setTitle(bdt.getTitle());
        blog.setBody(bdt.getBody());
        blog.setAuthor("admin");
        bd.save(blog);
    }

    public void hapus(String id){
        bd.deleteById(id);
    }
}
